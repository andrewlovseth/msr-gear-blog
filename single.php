<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<?php get_template_part('partials/single-post/featured-image'); ?>	

		<div class="article-body clearfix">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
			
					<?php get_template_part( 'content', 'single' ); ?>
					
				</main>
			</div>
			
			<?php get_sidebar(); ?>		
		</div>
		
		<div class="article-footer">
			<?php get_template_part('partials/single-post/products'); ?>

			<?php get_template_part('partials/single-post/comments'); ?>
		</div>
		
	<?php endwhile; ?>

<?php get_footer(); ?>
