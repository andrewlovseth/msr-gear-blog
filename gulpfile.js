'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

function errorlog(err) {
    console.error(err.message);
    this.emit('end');
}

function style() {
    return gulp
        .src('scss/style.scss')
        .pipe(sass().on('error', errorlog))
        .pipe(autoprefixer())
        .pipe(sourcemaps.init())
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        proxy: 'https://msrgearblog.local/',
    });

    gulp.watch('./scss/**/*.scss', style);

    gulp.watch('./*.php').on('change', browserSync.reload);
    gulp.watch('./partials/**/*.php').on('change', browserSync.reload);

    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;
