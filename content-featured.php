<?php
/**
 * @package Summit Register
 */
 
$featured = mytheme_get_featured_posts();
?>

<ul class="slides">
<?php foreach ( $featured as $post ) : setup_postdata( $post ); ?>  
	<?php $image_src = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); ?>
	<li class="featured-item" style="background-image:url( <?php echo $image_src ?> );">
		<div class="featured-overlay"></div>
		<div class="featured-text">
			<?php the_title( sprintf( '<h2 class="entry-title featured-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			<a class="read-more" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo __('Read More', 'msr') ?></a>
		</div>
	</li>
<?php endforeach; ?>  
</ul>
