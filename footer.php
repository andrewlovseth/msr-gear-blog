<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Summit Register
 */
?>

	</div><!-- #content -->
	
	<footer class="site-footer">
		<div class="columns">

			<?php if(have_rows('footer_link_columns', 'options')): while(have_rows('footer_link_columns', 'options')) : the_row(); ?>

				<?php if( get_row_layout() == 'column' ): ?>

					<div class="col">
						<div class="header">
							<h4><?php echo get_sub_field('column_header'); ?></h4>
						</div>

						<ul class="link-list">
							<?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
								<?php 
									$link = get_sub_field('link');
									if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
								 ?>

								 	<li><a class="<?php echo sanitize_title_with_dashes($link_title); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></li>

								<?php endif; ?>
							<?php endwhile; endif; ?>
						</ul>
						
					</div>

				<?php endif; ?>

			<?php endwhile; endif; ?>

			<div class="col social">
				<div class="header">
					<h4><?php echo get_field('social_header', 'options'); ?></h4>
				</div>

				<ul class="social-links">
					<?php if(have_rows('social', 'options')): while(have_rows('social', 'options')): the_row(); ?>

						<li><a class="<?php echo sanitize_title_with_dashes(get_sub_field('network')); ?>" href="<?php echo get_sub_field('link'); ?>" target="_blank"><img src="<?php echo get_sub_field('icon'); ?>" alt="<?php echo get_sub_field('network'); ?>" /></a></li>

					<?php endwhile; endif; ?>
				</ul>

				<div class="blog">
					<a href="<?php echo site_url(); ?>">
						<img src="<?php $image = get_field('blog_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>

				<div class="lang-switcher">
					<?php do_action('wpml_add_language_selector'); ?>
				</div>

				
			</div>			

		</div>
	</footer>

	<footer class="corporate-footer">

		<div class="global">
			<ul>
				<?php if(have_rows('global_footer', 'options')): while(have_rows('global_footer', 'options')): the_row(); ?>
				 
				    <?php 
						$link = get_sub_field('link');
						if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					 ?>

					 		<li><a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></li>
					<?php endif; ?>

				<?php endwhile; endif; ?>
			</ul>
		</div>

		<div class="copyright">
			<p><?php echo get_field('copyright', 'options'); ?></p>
		</div>

	</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
