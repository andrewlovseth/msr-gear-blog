<?php
/**
 * Summit Register functions and definitions
 *
 * @package Summit Register
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'msr_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function msr_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Summit Register, use a find and replace
	 * to change 'msr' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'msr', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	add_post_type_support('page', 'excerpt');

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'msr-post-grid-thumb', 800, 400, true );
	add_image_size( 'msr-post-hero', 1500, 1500, false );
	
	/*
	 * Change default image size to large
	 */
	add_filter( 'pre_option_image_default_size', 'msr_default_image_size' );
	function msr_default_image_size () {
	    return 'large'; 
	}
	
	/*
	 * Add custom expcertp length
	 */
	add_filter( 'excerpt_length', 'grid_excerpt_length', 999 );
	function grid_excerpt_length( $length ) {
		return 130;
	}
	
	add_filter('excerpt_more', 'new_excerpt_more');  
	function new_excerpt_more($text){  
	    return '...';  
	}  

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'msr' ),
		'footer' => __( 'Footer Menu')
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

function wpdocs_my_search_form( $form ) {
    $form = '<form role="search" method="get" id="search-form" class="search-form" action="' . home_url( '/' ) . '" >
    <input type="text" value="' . get_search_query() . '" name="s" id="s" class="search-input" placeholder="Search the blog" />
    <input type="submit" id="search-submit" class="search-submit" value="'. esc_attr__( 'Search' ) .'" />
    </form>';
 
    return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'msr_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	
	// $content_type = 'featured-content';
	// $filter_name = 'msr_get_featured_posts';
	// if(ICL_LANGUAGE_CODE == 'fr'){
	// 	$content_type = 'featured-fr-content';
	// 	$filter_name = 'msr_get_featured_posts_fr';
	// }else if(ICL_LANGUAGE_CODE == 'de'){
	// 	$content_type = 'featured-de-content';
	// 	$filter_name = 'msr_get_featured_posts_de';
	// }
	
	add_theme_support( 'featured-content', array(
	    'filter'     => 'msr_get_featured_posts',
	    'max_posts'  => 3,
	    'post_types' => array( 'post' ),
	) );

	if ( class_exists( 'Featured_Content' ) ) {
		Featured_Content::delete_transient();
	}
}
endif; // msr_setup
add_action( 'after_setup_theme', 'msr_setup' );

function mytheme_get_featured_posts() {
    return apply_filters( 'msr_get_featured_posts', array() );
}

function msr_has_featured_posts( $minimum = 1 ) {
    if ( is_paged() )
        return false;
 
    $minimum = absint( $minimum );
    $featured_posts = apply_filters( 'msr_get_featured_posts', array() );

    // echo '<pre>';
    // print_r($featured_posts);
    // echo '</pre>';
 
    if ( ! is_array( $featured_posts ) )
        return false;
 
    if ( $minimum > count( $featured_posts ) )
        return false;
 
    return true;
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function msr_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'msr' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'msr_widgets_init' );

/**
 * Add class to body if post has a featured image
 */
function add_featured_image_body_class( $classes ) {    
    global $post;

    if ( (is_single() || is_page()) && isset ( $post->ID ) ) {
	    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'msr-post-hero', true );  
		if( $image_url[1] >= 1000 ) {
	    	$classes[] = 'has-featured-image';
	    }
    } elseif (is_front_page() && !is_paged() ) {
    	$classes[] = 'has-featured-image';
	}

    return $classes;
}
add_filter( 'body_class', 'add_featured_image_body_class' );

/**
 * Change Jetpack Sharing buttons position
 */

function jptweak_remove_share() {
	remove_filter( 'the_content', 'sharing_display',19 );
	remove_filter( 'the_excerpt', 'sharing_display',19 );
}

add_action( 'loop_end', 'jptweak_remove_share' );

/**
 * Enqueue scripts and styles.
 */
function msr_scripts() {
	// wp_enqueue_style( 'msr-style', get_stylesheet_uri() );

    $style_last_updated_at = filemtime(get_stylesheet_directory() . '/style.css');

	wp_enqueue_style( 'msr-style', get_template_directory_uri() . '/style.css', '', $style_last_updated_at );

	wp_enqueue_script( 'msr-main', get_template_directory_uri() . '/js/msr.js', array(), '20220210-1', true );

	wp_enqueue_script( 'msr-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( msr_has_featured_posts( 2 ) ) {
		wp_enqueue_script( 'msr-flex-slider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array( 'jquery' ) );
	}
	
	if (!is_admin()) add_action("wp_enqueue_scripts", "msr_jquery_enqueue", 11);
	function msr_jquery_enqueue() {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", false, null);
	   wp_enqueue_script('jquery');
	}
}
add_action( 'wp_enqueue_scripts', 'msr_scripts' );

// register MSR_Follow_Social_Widget widget
function msr_register_follow_social_widget() {
    register_widget( 'MSR_Follow_Social_Widget' );
}
add_action( 'widgets_init', 'msr_register_follow_social_widget' );

function msr_insert_class( $classes ) {
	global $post;

	$desired_post =  get_option( 'msr_settings' );
	$desired_post = isset ( $desired_post ) ? $desired_post['msr_inject_page_id'] : null;

	//if( is_home() &&  ( 0 == get_query_var('paged') || 1 == get_query_var('paged') )  && $post->ID == $desired_post){
	if( is_home() &&  ( 0 == get_query_var('paged') || 1 == get_query_var('paged') ) ){
		/*if(ICL_LANGUAGE_CODE == 'fr' && $post->ID == 6158){
			$classes[] = 'entry-inserted';
		}else if(ICL_LANGUAGE_CODE == 'de' && $post->ID == 6163){
			$classes[] = 'entry-inserted';
		}else if(ICL_LANGUAGE_CODE == 'en' && $post->ID == $desired_post){
			$classes[] = 'entry-inserted';
		}*/
	}

	return $classes;
}
add_filter( 'post_class', 'msr_insert_class' );

function msr_insert_post_on_home( $posts ) {
	global $wp_query;

	$desired_post =  get_option( 'msr_settings' );
	$desired_post = isset ( $desired_post ) ? $desired_post['msr_inject_page_id'] : null;

	if (  is_home() &&  ( 0 == get_query_var('paged') || 1 == get_query_var('paged') ) ) {

		/*if(ICL_LANGUAGE_CODE == 'fr'){
			$p2insert = new WP_Query( array('page_id' => '6158', 'suppress_filters' => true) );
		}else if(ICL_LANGUAGE_CODE == 'de'){
			$p2insert = new WP_Query( array('page_id' => '6163', 'suppress_filters' => true) );
		}else if(ICL_LANGUAGE_CODE == 'en'){
		
			$p2insert = new WP_Query( array('page_id' => $desired_post, 'suppress_filters' => true) );
			$insert_at = 6;
			if ( !empty( $p2insert->posts ) ) {
				array_splice( $posts, $insert_at, 0, $p2insert->posts );
			}
    	}*/
  	}
  	return $posts;
}
add_filter( 'the_posts', 'msr_insert_post_on_home', 25 );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 *	Load social follow widgets
 */
require get_template_directory() . '/inc/widget-follow-social.php';

require get_template_directory() . '/settings.php';


// Add SVG Support
function bearsmith_add_svg_support($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'bearsmith_add_svg_support');



if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') ) {

	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Mobile Navigation');
	acf_add_options_sub_page('Desktop Navigation');
	acf_add_options_sub_page('Footer');
}
