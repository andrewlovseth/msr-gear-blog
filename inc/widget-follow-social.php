<?php 
/**
 * Adds MSR_Follow_Social_Widget widget.
 */
class MSR_Follow_Social_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'msr_follow_social_widget', // Base ID
			'MSR Follow us', // Name
			array( 'description' => 'MSR follow us on social networks widget' ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		?>
		<a class="msr-social-icon" href="https://www.facebook.com/msrgear" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook"></a><a class="msr-social-icon" href="http://twitter.com/msrgear/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.png" alt="twitter"></a><a class="msr-social-icon" href="http://www.youtube.com/MSRGear" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.png" alt="youtube"></a><a class="msr-social-icon" href="https://instagram.com/msr_gear/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-instagram.png" alt="Instagram"></a>
<!-- 		<a href="http://www.youtube.com/subscription_center?add_user=msrgear" target="_blank">Subscribe on Youtube</a> -->
		<?php
		echo $args['after_widget'];
	}

} // class MSR_Follow_Social_Widget