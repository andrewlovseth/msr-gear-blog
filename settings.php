<?php
class MSRSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'MSR', 
            'manage_options', 
            'msr-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'msr_settings' );
        ?>
        <div class="wrap">
            <h2>MSR Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'msr_settings_group' );   
                do_settings_sections( 'msr-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'msr_settings_group', // Option group
            'msr_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'MSR Custom Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'msr-setting-admin' // Page
        );  

        add_settings_field(
            'msr_inject_page_id', // ID
            'Page ID to insert in home page list', // Title 
            array( $this, 'msr_inject_page_id_callback' ), // Callback
            'msr-setting-admin', // Page
            'setting_section_id' // Section           
        );        
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {

        $new_input = array();
        if( isset( $input['msr_inject_page_id'] ) )
            $new_input['msr_inject_page_id'] = absint( $input['msr_inject_page_id'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function msr_inject_page_id_callback()
    {
        printf(
            '<input type="text" id="msr_inject_page_id" name="msr_settings[msr_inject_page_id]" value="%s" /><small>This inserts the given page into the homepage grid at a fixed position.</small>',
            isset( $this->options['msr_inject_page_id'] ) ? esc_attr( $this->options['msr_inject_page_id']) : ''
        );
    }
}

if( is_admin() )
    $msr_settings_page = new MSRSettingsPage();