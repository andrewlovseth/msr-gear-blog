<?php
/**
 * @package Summit Register
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<div class="entry-time">
				<?php the_time('M j, Y') ?> by <?php the_author() ?>
			</div>
			
			<div class="entry-categories">
				<?php
					$categories = get_the_category();
					$separator = ' ';
					$output = '';
					if($categories){
						foreach($categories as $category) {
							$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
						}
					echo trim($output, $separator);
					}
				?>
			</div>
			
			<?php 
			if( function_exists( 'sharing_display' ) ) {
				echo sharing_display(); 	
			}
			?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'msr' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<div class="entry-tags">
			<?php the_tags( 'Tags', '', '' ); ?> 
		</div>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<!-- <div id="scroll-wrapper">
			<div id="scroll-inner"><a id="top-scroll" href="#primary">TOP</a></div>
		</div> -->
		<div id="add-scroll"></div>
