<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Summit Register
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="google-site-verification" content="tHq3GG5-3669ER7szmZlrjHqvJ_fUrwc054u5Sa1go0" />
<?php get_template_part('partials/header/ga-data'); ?>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="/gfx/msr/favicon.png" rel="shortcut icon" type="image/ico" />
<script type="text/javascript" src="//use.typekit.net/bcd7gkl.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe
src="https://www.googletagmanager.com/ns.html?id=GTM-N9N8BTX"
height="0" width="0"
style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'msr' ); ?></a>

	<?php get_template_part('template-parts/header/search'); ?>

	<header class="site-header">

		<?php get_template_part('template-parts/header/nav-bar'); ?>

		<div class="site-navigation">
			<?php get_template_part('template-parts/header/hamburger'); ?>
			<?php get_template_part('template-parts/header/logo'); ?>
			<?php get_template_part('template-parts/header/main-navigation'); ?>
			<?php get_template_part('template-parts/header/utilities'); ?>
		</div>

		<?php // get_template_part('template-parts/header/banner'); ?>

	</header>

	<?php get_template_part('template-parts/header/mobile-navigation'); ?>

	<?php get_template_part('template-parts/header/blog-nav'); ?>
