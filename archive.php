<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summit Register
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

		<?php if ( have_posts() ) : ?>


			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>
			
			</main><!-- #main -->
			
			<div class="post-pagination">
				<?php
					the_posts_pagination( array(
						'screen_reader_text' => 'Archive',
						'format' => 'page/%#%/',
						'mid_size'  => 2,
						'prev_text' => __('Prev'),
						'next_text' => __('Next'),
					) );
				?>
			</div>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		<?php get_template_part('template-parts/footer/blog-search'); ?>

	</div><!-- #primary -->
	
<?php get_footer(); ?>

