<?php
/**
 * The template for displaying search results pages.
 *
 * @package Summit Register
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">



		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'msr' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

				<?php get_template_part('template-parts/footer/blog-search'); ?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );
				?>

			<?php endwhile; ?>
			
			</main><!-- #main -->
			
			<div class="post-pagination">
				<?php
					the_posts_pagination( array(
						'screen_reader_text' => 'Archive',
						'format' => '?paged=%#%',
						'mid_size'  => 2,
						'prev_text' => __('Prev'),
						'next_text' => __('Next'),
					) );
				?>
			</div>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		<?php get_template_part('template-parts/footer/blog-search'); ?>

	</div><!-- #primary -->
	
<?php get_footer(); ?>
