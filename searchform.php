<?php
/**
 * The template for displaying search results pages.
 *
 * @package Summit Register
 */
?>

<form action="<?php echo home_url( '/' ); ?>" method="get">
	<input type="text" name="s" id="search" placeholder=" <?php echo __('Search', 'msr') ?>" value="<?php the_search_query(); ?>" />
</form>
