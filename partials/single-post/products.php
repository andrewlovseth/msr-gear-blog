<?php $posts = get_field('products'); if( $posts ): ?>
	<section id="products">

		<h5>Products in this Post</h5>

		<div class="products-wrapper">

		    <?php foreach( $posts as $post): setup_postdata($post); ?>

		        <div class="product">
		        	<div class="photo">
		        		<a href="<?php echo get_field('link'); ?>" rel="external">
			        		<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			        	</a>
			        </div>

			        <div class="info">
			        	<h4><a href="<?php echo get_field('link'); ?>" rel="external"><?php the_title(); ?></a></h4>
			        	<a href="<?php echo get_field('link'); ?>" rel="external" class="shop">Shop</a>
			        </div>	        
		        </div>

		    <?php endforeach; ?>

		</div>

	</section>
<?php wp_reset_postdata(); endif; ?>





