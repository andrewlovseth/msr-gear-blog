<?php if ( comments_open() || get_comments_number() ) : ?>
    <div class="article-comments">
        <?php comments_template(); ?>
    </div>
<?php endif; ?>