<?php if ( has_post_thumbnail() ): ?>
    
    <?php
        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'msr-post-hero', true );
        if( $image_url && isset ( $image_url[0] ) && $image_url[1] >= 1000 ): 
    ?>
    
        <div class="article-featured-image" style="background-image: url('<?php echo $image_url[0]; ?>');">
            <div class="featured-overlay"></div>
        </div>

    <?php endif; ?>
    
<?php endif; ?>