<nav class="blog-nav">

	<div class="blog-nav-header">
		<div class="logo">
			<a href="<?php echo site_url(); ?>">
				<img src="<?php bloginfo('template_directory'); ?>/img/summit-register-logo.png" alt="The Summit Register" />
			</a>
		</div>

		<div class="mobile-toggle">
			<a href="#" class="blog-nav-trigger">
				<img src="<?php bloginfo('template_directory'); ?>/img/icon-down-arrow.svg" alt="Menu Toggle" />
			</a>
		</div>
	</div>

	<div class="blog-nav-links">

		<?php if(have_rows('blog_navigation', 'options')): while(have_rows('blog_navigation', 'options')): the_row(); ?>

			<?php 
				$link = get_sub_field('link');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			 ?>

			 	<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

			<?php endif; ?>

		<?php endwhile; endif; ?>

		<div class="search-btn closed">
			<a href="#" class="search-trigger search-trigger-open"><img src="<?php bloginfo('template_directory'); ?>/img/icon-search.svg" alt="Search" /></a>
			<a href="#" class="search-trigger search-trigger-closed"><img src="<?php bloginfo('template_directory'); ?>/img/icon-search.svg" alt="Search" /></a>

			<div class="form-wrapper">
				<?php echo get_search_form(); ?>
			</div>				
		</div>
		
	</div>

</nav>