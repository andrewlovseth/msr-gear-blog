<nav class="main">
	<div class="hamburger-btn">
		<a href="#">☰</a>
	</div>

	<div class="site-logo">
		<a href="https://www.msrgear.com/">
			<img src="<?php $image = get_field('site_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="user-links">
		<?php //do_action('wpml_add_language_selector'); ?>

		<div class="account">
			<a href="<?php echo get_field('account_link', 'options'); ?>">
				<i class="fa fa-user" aria-hidden="true"></i>
			</a>
		</div>

		<div class="cart">
			<a href="<?php echo get_field('cart_link', 'options'); ?>">
				<i class="minicart-icon fa fa-shopping-bag"></i>
			</a>
		</div>
		
	</div>

	<div class="site-search">
		<form class="search-form">
			<input type="search" class="search-input" placeholder="Search (keywords,etc)" />
			<button class="submit fa fa-search cursor"></button>
		</form>		
	</div>

	<?php get_template_part('partials/header/site-navigation'); ?>
</nav>