<?php if(get_field('show_announcement_banner', 'options')): ?>

	<section class="announcement-banner">
		<a href="#" class="announcement-trigger"><?php echo get_field('announcement_banner', 'options'); ?></a>
	</section>

	<article class="announcement-overlay">
		<span class="close announcement-close">×</span>
		
		<div class="info-wrapper">
			<div class="info">
				<?php echo get_field('announcement_banner_overlay', 'options'); ?>
			</div>					
		</div>
	</article>

<?php endif; ?>