<?php
    // Country Code
   /* $ip_obj = geoip_detect2_get_info_from_ip($_SERVER['REMOTE_ADDR']);
    $country = $ip_obj->raw['country']['iso_code'];
    if($country !== NULL) {
        $country_code = $country;
    } else {*/
        $country_code = "US";
    //}    

    // Currency Code
    /*if(ICL_LANGUAGE_CODE == 'fr' || ICL_LANGUAGE_CODE == 'de') {
        $currency_code = 'EUR';
    } else {*/
        $currency_code = 'USD';
    //}
?>

<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    'event': 'dataLayer-loaded',
    'country': '<?php echo $country_code; ?>',
    'currencyCode': '<?php echo $currency_code; ?>',
    'brand': 'msr',
    'customerType': 'unregistered', 
    'pageType': 'blog'});
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})
(window,document,'script','dataLayer','GTM-N9N8BTX');</script>
<!-- End Google Tag Manager -->