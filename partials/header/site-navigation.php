<div class="site-nav">
	<?php if(have_rows('site_navigation', 'options')): ?>

		<ul class="navbar">

			<?php while(have_rows('site_navigation', 'options')) : the_row(); ?>

				<?php if( get_row_layout() == 'dropdown' ): ?>

					<li class="menu-item dropdown">
						<a class="nav-link dropdown-link" href="#"><?php echo get_sub_field('label'); ?></a>

						<?php $three_col = get_sub_field('three_col_layout'); if(have_rows('link_groups')): ?>
						 
						    <ul class="dropdown-nav<?php if($three_col): ?> three-col<?php endif; ?>">
						    	<?php while(have_rows('link_groups')): the_row(); ?>

							    	<li class="link-group">
							    		<?php 
											$link = get_sub_field('main_link');
											if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
										 ?>

											 <a class="link-group-header" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

										<?php endif; ?>

										<?php if(have_rows('sub_links')): ?>

											<ul class="sub-links">

												<?php while(have_rows('sub_links')): the_row(); ?>

													<li>
														<?php 
															$link = get_sub_field('link');
															if( $link ): 
															$link_url = $link['url'];
															$link_title = $link['title'];
															$link_target = $link['target'] ? $link['target'] : '_self';
														 ?>
														 		<a class="sub-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

														<?php endif; ?>
													</li>
												
												<?php endwhile;?>

											</ul>

										<?php endif; ?>

							    	</li>

					    		<?php endwhile; ?>
					    	</ul>
					
						<?php endif; ?>
					</li>

				<?php endif; ?>

				<?php if( get_row_layout() == 'basic_link' ): ?>

					<li class="menu-item">
						<?php 
							$link = get_sub_field('link');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						 ?>

						 	<a class="nav-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

						<?php endif; ?>							
					</li>

				<?php endif; ?>

			<?php endwhile; ?>

		</ul>

	<?php endif; ?>	
</div>