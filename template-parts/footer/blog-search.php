<div class="blog-search">
    <div class="wrapper">
        <div class="search-header">
            <h4>Search The Summit Register Blog</h4>
        </div>

        <div class="form">
            <?php echo get_search_form(); ?>
        </div>        
    </div>
</div>