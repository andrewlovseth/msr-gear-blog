<?php if(have_rows('main_navigation', 'options')): ?>
    <nav class="desktop-nav">
        <ul>
            <?php while(have_rows('main_navigation', 'options')) : the_row(); ?>

                <?php if( get_row_layout() == 'basic_link' ): ?>                        
                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                        <li class="first-level basic-link <?php echo sanitize_title_with_dashes($link_title); ?>">
                            <a class="main-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        </li>

                    <?php endif; ?>
                <?php endif; ?>

                <?php if( get_row_layout() == 'dropdown' ): ?>

                    <li class="first-level dropdown-link <?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
                        <a href="#<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>" class="main-link"><?php echo get_sub_field('label'); ?></a>

                        <div class="dropdown-menu">
                            <div class="dropdown-menu-container">
                                <?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>

                                    <?php
                                        $link_group = get_sub_field('link_group');
                                        $header = $link_group['header'];
                                    ?>

                                    <?php if(get_sub_field('type') == 'links'): ?>
                                        <?php if(have_rows('link_group')): while(have_rows('link_group')): the_row(); ?>

                                            <div class="col links">
                                                <h2><?php echo $header; ?></h2>

                                                <?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
                                                    <?php 
                                                        $link = get_sub_field('link');
                                                        if( $link ): 
                                                        $link_url = $link['url'];
                                                        $link_title = $link['title'];
                                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                                    ?>

                                                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

                                                    <?php endif; ?>

                                                <?php endwhile; endif; ?>
                                            </div>

                                        <?php endwhile; endif; ?>
                                    <?php endif; ?>

                                    <?php if(get_sub_field('type') == 'promo'): ?>

                                        <?php 
                                            $promo = get_sub_field('promo');
                                            $link = $promo['link'];
                                            $photo = $promo['photo'];
                                        ?>

                                        <div class="col promo">
                                            <div class="promo-container">
                                                <div class="content">
                                                    <?php 
                                                        if( $link ): 
                                                        $link_url = $link['url'];
                                                        $link_title = $link['title'];
                                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                                    ?>

                                                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                                            <div class="info">
                                                                <span class="label"><?php echo esc_html($link_title); ?></span>
                                                                <span class="explore">Explore</span>                                                                    
                                                            </div>

                                                            <?php echo wp_get_attachment_image($photo['ID'], 'large'); ?>
                                                        </a>

                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    <?php endif; ?>

                                <?php endwhile; endif; ?>
                            </div>
                        </div>                            
                    </li>

                <?php endif; ?>

            <?php endwhile; ?>
        </ul>
    </nav>
<?php endif; ?>