<div class="utilities">
    
    <div class="search">
        <div class="mobile">
            <a href="#" class="js-site-search-toggle"></a>
        </div>

        <div class="desktop">
            <div class="search-container js-site-search-toggle">
                <button class="search-btn"></button>
                <input class="search-field" placeholder="Search" type="search" />
            </div>
        </div>
    </div>

    <div class="links">
        <div class="profile">
            <a href="https://www.msrgear.com/login"></a>
        </div>

        <div class="cart">
            <a href="https://www.msrgear.com/cart"></a>
        </div>
    </div>



</div>